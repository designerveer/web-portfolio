import { Component, OnInit } from '@angular/core';

@Component({
	selector: 'app-pricing',
	templateUrl: './pricing.component.html',
	styleUrls: ['./pricing.component.scss']
})
export class PricingComponent implements OnInit {

	//NumPrices = [1,2,3,4];
	prices : any;

	constructor() { 
		this.prices = [
		{
			number : '1',
			title : 'Economy',
			subTitle : 'For the individuals',
			point : [
				'Secure Online Transfer',
				'Unlimited Styles for interface',
				'Reliable Customer Service'
			],
			price : '$199'

		},
		{
			number : '2',
			title : 'General',
			subTitle : 'For the Dual',
			point : [
				'Double Secure Online Transfer',
				'Unlimited Styles for interface',
				'Reliable Customer Service'
			],
			price : '$299'

		},
		{
			number : '3',
			title : 'Gold',
			subTitle : 'For the Four',
			point : [
				'Multi Secure Online Transfer',
				'Unlimited Styles for interface',
				'Reliable Customer Service'
			],
			price : '$399'

		},
		{
			number : '4',
			title : 'Primeum',
			subTitle : 'For the Multi User',
			point : [
				'Unlimited Secure Online Transfer',
				'Unlimited Styles for interface',
				'Reliable Customer Service'
			],
			price : '$299'

		},
		{
			number : '5',
			title : 'legend',
			subTitle : 'For the Multi User',
			point : [
				'Unlimited Secure Online Transfer',
				'Unlimited Styles for interface',
				'Reliable Customer Service'
			],
			price : '$299'

		}
		]
	}

	ngOnInit() {
	}

}
